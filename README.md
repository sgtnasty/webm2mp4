# webm2mp4

webm2mp4 is a bash script to convert webm files to mp4 via ffmpeg.

## Requirements

1. POSIX compatible system with bash
2. [ffmpeg](http://ffmpeg.org) installed on system
3. [git](https://git-scm.com) installed on system

## Installation

There is no formal installation for this script. Simply clone the repo and try it out. Permissions should be already set to execute.

```bash
git clone https://gitlab.com/sgtnasty/webm2mp4.git
```

## Usage

```bash
# change to the directory to where the input files are
cd /path/to/files
# NOT IN PATH YET, so i guess I need an install...
# run the script, and follow the prompt to continue
webm2mp4
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Todo

1. optional command line arguments
2. args for source dir
3. args for output dir
4. arg to delete source file AFTER sucessful convert
5. args for debug

## License

[GPL3](https://choosealicense.com/licenses/gpl-3.0/)